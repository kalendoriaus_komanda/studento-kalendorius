//
//  PrisijungimasController.swift
//  StudentoKalendorius
//
//  Created by Giedrius Visokinskas on 08/12/15.
//  Copyright © 2015 Giedrius Visokinskas. All rights reserved.
//

import Foundation
import UIKit

struct Vartotojas {
    static var vardas = "",    slapt = ""
}

class PrisjungimasController : UIViewController {
    
    @IBOutlet weak var vardas: UITextField!
    
    @IBOutlet weak var slapt: UITextField!
    
    @IBOutlet weak var klaida: UILabel!
    
    @IBAction func prisijungimasClick(sender: UIButton) {
        self.klaida.hidden = true;
        if (self.vardas.text!.isEmpty == true || self.slapt!.text?.isEmpty == true) {
            return
        }
        request(.GET, "http://localhost:4567/api/v1/vartotojas/egzistuoja/" + self.vardas.text! + "/" + self.slapt.text!)
            .responseString { response in
                if (response.result.value != "false") {
                    Vartotojas.vardas = self.vardas.text!
                    Vartotojas.slapt = self.slapt.text!
                    let storyboard = UIStoryboard(name: "Pagrindinis", bundle: nil)
                    let vc = storyboard.instantiateInitialViewController()! as! LoadingViewController
                    self.presentViewController(vc, animated: true, completion: nil)
                    vc.loadCustomView(0)
                } else {
                    self.klaida.hidden = false;
                }
        }
    }
}
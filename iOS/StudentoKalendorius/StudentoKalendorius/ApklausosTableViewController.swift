//
//  ApklausosTableViewController.swift
//  StudentoKalendorius
//
//  Created by Giedrius Visokinskas on 09/12/15.
//  Copyright © 2015 Giedrius Visokinskas. All rights reserved.
//

import UIKit

class ApklausosTableViewController: UITableViewController {
    
    var apklausos = [Apklausa]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareData()
        self.tableView.contentInset = UIEdgeInsetsMake(20.0, 0.0, 0.0, 0.0);
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return apklausos.count
    }
    
    func prepareData() {
        let json = Apklausos.json
                
        print("JSON: \(json)")
        
        for (_, object) in json {
            let a  = Apklausa()

            a.nuoKo = object["nuoKo"].stringValue
            a.tekstas = object["tekstas"].stringValue
            a.id = object["id"].stringValue
            
            apklausos += [a]
        }
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("apklausosCele", forIndexPath: indexPath) as! ApklausosTableViewCell

        
        let apklausa = apklausos[indexPath.row]
        
        cell.nuoKo.text = apklausa.nuoKo
        cell.tekstas.text = apklausa.tekstas
        cell.id = apklausa.id
                
        return cell
    }
    
}

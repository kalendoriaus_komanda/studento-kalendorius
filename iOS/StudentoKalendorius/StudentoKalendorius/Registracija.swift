//
//  Registracija.swift
//  StudentoKalendorius
//
//  Created by Giedrius Visokinskas on 08/12/15.
//  Copyright © 2015 Giedrius Visokinskas. All rights reserved.
//

import UIKit

class Registracija : UIViewController {
    
    @IBOutlet weak var vardas: UITextField!
    @IBOutlet weak var pavarde: UITextField!
    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var slapt: UITextField!
    @IBOutlet weak var studProg: UITextField!
    @IBOutlet weak var kursas: UITextField!
    @IBOutlet weak var grupe: UITextField!
    @IBOutlet weak var pogrupis: UITextField!
    @IBOutlet weak var klaida: UILabel!
    
    @IBAction func registruotis(sender: AnyObject) {
        self.klaida.hidden = true;
        request(.POST, "http://localhost:4567/api/v1/studentas/registruoti/" + self.userName.text! + "/" + self.slapt.text! + "/" + self.vardas.text! + "/" + self.pavarde.text! + "/" + self.studProg.text! + "/" + self.kursas.text! + "/" + self.grupe.text! + "/" + self.pogrupis.text!)
            .responseData { response in
                print(response.request)
                if (response.response?.statusCode != 500) {
                    self.navigationController?.popToRootViewControllerAnimated(true)
                } else {
                    self.klaida.hidden = false;
                }
        }
    }
}

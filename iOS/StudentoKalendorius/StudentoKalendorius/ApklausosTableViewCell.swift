//
//  ApklausosTableViewCell.swift
//  StudentoKalendorius
//
//  Created by Giedrius Visokinskas on 09/12/15.
//  Copyright © 2015 Giedrius Visokinskas. All rights reserved.
//

import UIKit

class ApklausosTableViewCell: UITableViewCell {

    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var nuoKo: UILabel!
    @IBOutlet weak var tekstas: UILabel!
    var id = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func atsakytiTaip(sender: AnyObject) {
        spinner.hidden = false
        spinner.startAnimating()
        request(.POST, "http://localhost:4567/api/v1/apklausa/atsakyti/" + Vartotojas.vardas + "/" + self.id + "/Taip" )
            .responseData { response in
                print(response.request)
                if (response.response?.statusCode != 500) {
                    self.spinner.stopAnimating()
                    self.spinner.hidden = true
                } else {
                    
                }
        }

    }
    @IBAction func atsakytiNe(sender: AnyObject) {
        spinner.hidden = false
        spinner.startAnimating()
        request(.POST, "http://localhost:4567/api/v1/apklausa/atsakyti/" + Vartotojas.vardas + "/" + self.id + "/Ne" )
            .responseData { response in
                print(response.request)
                if (response.response?.statusCode != 500) {
                    self.spinner.stopAnimating()
                    self.spinner.hidden = true
                } else {
                    
                }
        }
    }
}

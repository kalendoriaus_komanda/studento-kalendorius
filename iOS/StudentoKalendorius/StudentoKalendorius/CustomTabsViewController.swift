//
//  CustomTabsViewController.swift
//  StudentoKalendorius
//
//  Created by Giedrius Visokinskas on 08/12/15.
//  Copyright © 2015 Giedrius Visokinskas. All rights reserved.
//

import UIKit

class CustomTabsViewController: UITabBarController, UITabBarControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = self
    }
        
    //Delegate methods
    func tabBarController(tabBarController: UITabBarController, shouldSelectViewController viewController: UIViewController) -> Bool {
        print("Should select viewController: \(viewController.restorationIdentifier) ?")
        let vc = self.storyboard!.instantiateInitialViewController()! as! LoadingViewController
        self.presentViewController(vc, animated: true, completion: nil)
        vc.loadCustomView((self.viewControllers?.indexOf(viewController))!)
        return false;
    }

}

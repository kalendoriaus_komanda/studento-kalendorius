//
//  PranesimoTableViewController.swift
//  StudentoKalendorius
//
//  Created by Giedrius Visokinskas on 09/12/15.
//  Copyright © 2015 Giedrius Visokinskas. All rights reserved.
//

import UIKit

class PranesimoTableViewController: UITableViewController {
   
    var pranesimai = [Pranesimas]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareData()
        self.tableView.contentInset = UIEdgeInsetsMake(20.0, 0.0, 0.0, 0.0);
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pranesimai.count
    }

   
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("pranesimoCele", forIndexPath: indexPath) as! PranesimoTableViewCell

        let pranesimas = pranesimai[indexPath.row]
        
        cell.nuoKo.text = pranesimas.nuoKo
        cell.tekstas.text = pranesimas.tekstas
        
        return cell
    }

    func prepareData() {
        let json = Pranesimai.json
        
        print("JSON: \(json)")
        
        for (_, object) in json {
            let p  = Pranesimas()
            
            p.nuoKo = object["nuoKo"].stringValue
            p.tekstas = object["tekstas"].stringValue
            
            pranesimai += [p]
        }

    }
}

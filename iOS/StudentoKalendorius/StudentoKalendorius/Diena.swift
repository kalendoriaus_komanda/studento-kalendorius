//
//  FirstViewController.swift
//  StudentoKalendorius
//
//  Created by Giedrius Visokinskas on 08/12/15.
//  Copyright © 2015 Giedrius Visokinskas. All rights reserved.
//

import UIKit

class Diena: UITableViewController {

    @IBOutlet var lentele: UITableView!

    var paskaitos = [Paskaita]()
    var kontroliniai = [Kontrolinis]()
   
    override func viewDidLoad() {
        super.viewDidLoad()        
        paruoskDuomenis()
    }
    
    func paruoskDuomenis() {
        let json = DienosDuomenys.json
        
        let headeris=UILabel(frame: CGRectMake(100, 200, 100, 100))
        headeris.text = json["diena"].string
        headeris.textAlignment = NSTextAlignment.Center
        
        self.lentele.tableHeaderView = headeris
        print("JSON: \(json)")
        
        var p  = Paskaita()
        for (_, subJson) in json["paskaitos"] {
            if let title = subJson["laikas"].string {
                p = Paskaita()
                self.paskaitos += [p]
                p.laikas = title
            }
            if let title = subJson["pavadinimas"].string {
                p.pavadinimas = title
            }
            if let title = subJson["kabinetas"].string {
                p.kabinetas = title
            }
            if let title = subJson["destytojas"].string {
                p.destytojas = title
            }
        }
        
        for (_, object) in json["kontroliniai"] {
            let k  = Kontrolinis()
            
            k.destytojas = object["destytojas"].stringValue
            k.detales = object["detales"].stringValue
            k.kabinetas = object["kabinetas"].stringValue
            k.laikas = object["laikas"]["time"]["hour"].stringValue + ":" + String(format: "%02d", object["laikas"]["time"]["minute"].intValue)
            k.paskaitosPavadinimas = object["paskaitosPavadinimas"].stringValue
            
            kontroliniai += [k]
        }

    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return paskaitos.count + kontroliniai.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if (indexPath.row < paskaitos.count) {
            let cell = tableView.dequeueReusableCellWithIdentifier("PaskaitosCele", forIndexPath: indexPath) as! PaskaitosCele
            
            let paskaita = paskaitos[indexPath.row]
            
            cell.pavadinimas.text = paskaita.pavadinimas
            cell.destytojas.text = paskaita.destytojas
            cell.kabinetas.text = paskaita.kabinetas
            cell.laikas.text = paskaita.laikas
            
            cell.backgroundColor = UIColor(netHex: 0xd9edf7)
            
            return cell
        } else {
            let cell = tableView.dequeueReusableCellWithIdentifier("kontrolinioCele", forIndexPath: indexPath) as! KontrolinioCele
            
            let kontrolinis = kontroliniai[indexPath.row - paskaitos.count]
            
            cell.laikas.text = kontrolinis.laikas
            cell.kabinetas.text = kontrolinis.kabinetas
            cell.paskaitosPavadinimas.text = kontrolinis.paskaitosPavadinimas
            cell.destytojas.text = kontrolinis.destytojas
            cell.detales.text = kontrolinis.detales
            
            cell.backgroundColor = UIColor(netHex: 0xfcf8e3)
            
            return cell

        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }


}


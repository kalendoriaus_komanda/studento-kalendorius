//
//  KontrolinioCele.swift
//  StudentoKalendorius
//
//  Created by Giedrius Visokinskas on 10/12/15.
//  Copyright © 2015 Giedrius Visokinskas. All rights reserved.
//

import Foundation
import UIKit

class KontrolinioCele: UITableViewCell {
    
    @IBOutlet weak var laikas: UILabel!
    @IBOutlet weak var kabinetas: UILabel!
    @IBOutlet weak var paskaitosPavadinimas: UILabel!
    @IBOutlet weak var destytojas: UILabel!
    @IBOutlet weak var detales: UILabel!
}

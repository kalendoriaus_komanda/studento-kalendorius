//
//  LoadingViewController.swift
//  StudentoKalendorius
//
//  Created by Giedrius Visokinskas on 08/12/15.
//  Copyright © 2015 Giedrius Visokinskas. All rights reserved.
//

import UIKit
import Darwin

struct DienosDuomenys {
    static var json = JSON("")
}

struct Apklausos {
    static var json = JSON("")
}

struct Pranesimai {
    static var json = JSON("")
}

class LoadingViewController: UIViewController {
    
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if (Vartotojas.vardas == "" || Vartotojas.slapt == "") {
            exit(0)
        }
        spinner.startAnimating()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadCustomView(index : Int){
        switch index {
        case 0:
            request(.GET, "http://localhost:4567/api/v1/studentas/diena/" + Vartotojas.vardas + "/" + Vartotojas.slapt)
                .responseJSON { response in
                    if let result = response.result.value {
                        DienosDuomenys.json = JSON(result)
                    }
                    self.changeView(index)
            }
        case 1:
            request(.GET, "http://localhost:4567/api/v1/studentas/apklausa/" + Vartotojas.vardas + "/" + Vartotojas.slapt)
                .responseJSON { response in
                    if let result = response.result.value {
                        Apklausos.json = JSON(result)
                    }
                    self.changeView(index)
            }
        case 2:
            request(.GET, "http://localhost:4567/api/v1/studentas/pranesimas/" + Vartotojas.vardas + "/" + Vartotojas.slapt)
                .responseJSON { response in
                    if let result = response.result.value {
                        Pranesimai.json = JSON(result)
                    }
                    self.changeView(index)
            }
        default:
            exit(0)
        }
    }
    
    func changeView(index : Int) {
        let vc = self.storyboard!.instantiateViewControllerWithIdentifier("tabsControl") as! CustomTabsViewController
        vc.selectedIndex = index
        self.presentViewController(vc, animated: true, completion: nil)
    }
    
}

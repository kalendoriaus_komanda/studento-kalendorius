//
//  Paskaita.swift
//  StudentoKalendorius
//
//  Created by Giedrius Visokinskas on 08/12/15.
//  Copyright © 2015 Giedrius Visokinskas. All rights reserved.
//

import Foundation
import UIKit

class PaskaitosCele : UITableViewCell {
    
    @IBOutlet weak var laikas: UILabel!
    @IBOutlet weak var pavadinimas: UILabel!
    @IBOutlet weak var destytojas: UILabel!
    @IBOutlet weak var kabinetas: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
            super.setSelected(selected, animated: animated)
    }
}
//
//  PranesimoTableViewCell.swift
//  StudentoKalendorius
//
//  Created by Giedrius Visokinskas on 09/12/15.
//  Copyright © 2015 Giedrius Visokinskas. All rights reserved.
//

import UIKit

class PranesimoTableViewCell: UITableViewCell {

    @IBOutlet weak var tekstas: UILabel!
    @IBOutlet weak var nuoKo: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

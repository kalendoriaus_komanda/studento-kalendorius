GET:
Visu paskaitu gavimas:
localhost:4567/api/v1/studentas/paskaitos/PS/2/4
localhost:4567/api/v1/destytojas/paskaitos/Karolis/Uosis

Dienos generavimas:
localhost:4567/api/v1/studentas/diena/qa/qa
localhost:4567/api/v1//destytojas/diena/:vartotojoVardas/:slaptazodis

Vartotojo egzistavimo patikrinimas:
localhost:4567/api/v1/vartotojas/egzistuoja/qa/qa

Apklausu gavimas:
localhost:4567/api/v1/studentas/apklausa/qa/qa
localhost:4567/api/v1/destytojas/apklausa/Destytojas

Pranesimu gavimas:
localhost:4567/api/v1/studentas/pranesimas/qa/qa

Destytojo studentai:
localhost:4567/api/v1/destytojas/studentai/:vartotojoVardas/:slaptazodis

Destytojo grupes
localhost:4567/api/v1/destytojas/grupes/:vartotojoVardas/:slaptazodis

POST:
Vartotojo pridejimas:
localhost:4567/api/v1/studentas/registruoti/qa/qa/qa/qa/PS/2/4/2
localhost:4567/destytojas/registruoti/:vartotojoVardas/:slaptazodis/:vardas/:pavarde/

Pasirinktines veiklos pridejimas:
localhost:4567/api/v1/vartotojas/pridetiVeikla/qa/qa/2015/11/16/23:00:00/Bandymas/Bandau

Pranesimo kurimas:
localhost:4567/api/v1/pranesimas/Destytojas/qa - request body turetu buti zinute

Apklausos kurimas:
localhost:4567/api/v1/apklausa/Destytojas/qa  - request body turetu buti apklausa

Atsakymas i apklausa(tik studentui):
localhost:4567/api/v1/apklausa/atsakyti/qa/1/Taip

DELETE:
localhost:4567/api/v1/papildomaVeikla/istrinti/3
localhost:4567/api/v1/vartotojas/istrinti/1

Destytojo sasaja: localhost:4567/
var app = angular.module('destytojoSasaja', [
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ngRoute',
    'ui.bootstrap',
    'angularSpinner',
    'ngLoadingSpinner'
]);

app.config(function ($routeProvider) {
    $routeProvider.when('/', {
        templateUrl: 'views/prisijungti.html',
        controller: 'prisijungimas'
    }).when('/diena', {
        templateUrl: 'views/diena.html',
        controller: 'diena'
    }).when('/registruotis', {
        templateUrl: 'views/registruotis.html',
        controller: 'registruotis'
    }).when('/pranesimai', {
        templateUrl: 'views/pranesimai.html',
        controller: 'pranesimai'
    }).when('/apklausos', {
        templateUrl: 'views/apklausos.html',
        controller: 'apklausos'
    }).otherwise({
        redirectTo: '/'
    })
});

app.controller('prisijungimas', function ($scope, $http, $location, $log, $cookies, $rootScope, $cookieStore) {
    if ($cookies.prisijunges == "true") {
        $location.path('/diena');
    }

    $scope.login = function () {
        $http({
            method: 'GET',
            url: '/api/v1/vartotojas/egzistuoja/' + $scope.vardas + '/' + $scope.slapt
        }).then(function successCallback(response) {
            $log.log(response.data);
            if (response.data.toString() == 'false') {
                $scope.klaida = true;
            } else {
                $location.path('/diena');
                $cookies.vardas = $scope.vardas;
                $cookies.slapt = $scope.slapt;
                $cookies.prisijunges = "true";
                $rootScope.$broadcast("prisijungiau");
            }
        }, function errorCallback(response) {
        });
    }
});

app.controller('registruotis', function ($scope, $http, $location, $log, $cookies, $rootScope) {
    $scope.register = function () {
        $http({
            method: 'POST',
            url: '/api/v1/destytojas/registruoti/' + $scope.vardas + '/' + $scope.slapt + '/' + $scope.tikrasVardas + '/' + $scope.pavarde
        }).then(function successCallback(response) {
            $log.log(response.data);
            $cookies.vardas = $scope.vardas;
            $cookies.slapt = $scope.slapt;
            $cookies.prisijunges = "true";
            $location.path('/diena');
            $rootScope.$broadcast("prisijungiau");
        }, function errorCallback(response) {
            $scope.klaida = true;
        });
    }
});

app.controller('diena', function ($scope, $http, $location, $log, $cookies, $modal, $cookieStore, $rootScope) {
    if (!$cookies.prisijunges) {
        $location.path('/');
    }
    $http({
        method: 'GET',
        url: '/api/v1/destytojas/diena/' + $cookies.vardas + '/' + $cookies.slapt
    }).then(function successCallback(response) {
        $log.log(response.data);
        $scope.dienosDuomenys = response.data;
    }, function errorCallback(response) {
        $cookieStore.remove('prisijunges');
        $cookieStore.remove('vardas');
        $cookieStore.remove('pavarde');
        $rootScope.$broadcast("nerVartotojo");
        $location.path('/');
    });

    $scope.atidaryk = function () {
        $modal.open({
            animation: true,
            templateUrl: 'views/pridetiVeikla.html',
            controller: 'PridetiVeikla',
        });

    };

    $scope.pridetiKontrolini = function () {
        $modal.open({
            animation: true,
            templateUrl: 'views/pridetiKontrolini.html',
            controller: 'pridetiKontrolini',
        });

    };

    $scope.$on("naujaVeikla", function () {
        $http({
            method: 'GET',
            url: '/api/v1/destytojas/diena/' + $cookies.vardas + '/' + $cookies.slapt
        }).then(function successCallback(response) {
            $scope.dienosDuomenys = response.data;
        });
    });
});

app.controller('PridetiVeikla', function ($modalInstance, $scope, $log, $location, $cookies, $http, $rootScope) {
    if (!$cookies.prisijunges) {
        $location.path('/');
    }
    $scope.prideti = function () {
        $http({
            method: 'POST',
            url: '/api/v1/vartotojas/pridetiVeikla/' + $cookies.vardas + '/' + $cookies.slapt,
            data: this.veikla
        }).then(function successCallback(response) {
            $log.log("Pridėjau veiklą!");
            $rootScope.$broadcast("naujaVeikla");
            $modalInstance.dismiss('cancel');
        }, function errorCallback(response) {
        });
    }
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    }
});

app.controller('pridetiKontrolini', function ($modalInstance, $scope, $log, $location, $cookies, $http, $rootScope) {
    if (!$cookies.prisijunges) {
        $location.path('/');
    }

    $http({
        method: 'GET',
        url: '/api/v1/destytojas/grupes/' + $cookies.vardas + '/' + $cookies.slapt,
    }).then(function successCallback(response) {
        $scope.grupes = response.data;
    }, function errorCallback(response) {
    });

    $scope.prideti = function () {
        $http({
            method: 'POST',
            url: '/api/v1/kontrolinis/' + $cookies.vardas + '/' + $cookies.slapt,
            data: this.kontrolinis
        }).then(function successCallback(response) {
            $log.log("Pridėjau kontrolinį!");
            $rootScope.$broadcast("naujaVeikla");
            $modalInstance.dismiss('cancel');
        }, function errorCallback(response) {
        });
    };
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

    $scope.sukonstruokPavadinima = function (kursas, grupe, pogrupis) {
        pradzia = kursas + "k. " + grupe + " grupė ";
        return pogrupis > 0 ? pradzia + pogrupis + " pogrupis" : pradzia;
    };
});

app.filter('numberFixedLen', function () {
    return function (n, len) {
        var num = parseInt(n, 10);
        len = parseInt(len, 10);
        if (isNaN(num) || isNaN(len)) {
            return n;
        }
        num = '' + num;
        while (num.length < len) {
            num = '0' + num;
        }
        return num;
    };
});

app.controller('pranesimai', function ($scope, $log, $location, $cookies, $http) {
    if (!$cookies.prisijunges) {
        $location.path('/');
    }

    $http({
        method: 'GET',
        url: '/api/v1/destytojas/grupes/' + $cookies.vardas + '/' + $cookies.slapt,
    }).then(function successCallback(response) {
        $scope.grupes = response.data;
    }, function errorCallback(response) {
    });

    $scope.prideti = function () {
        $http({
            method: 'POST',
            url: '/api/v1/pranesimas/' + $cookies.vardas + '/' + $cookies.slapt,
            data: $scope.pranesimas
        }).then(function successCallback(response) {
        }, function errorCallback(response) {
        });

    };

    $scope.sukonstruokPavadinima = function (kursas, grupe, pogrupis) {
        pradzia = kursas + "k. " + grupe + " grupė ";
        return pogrupis > 0 ? pradzia + pogrupis + " pogrupis" : pradzia;
    };
});

app.controller('apklausos', function ($scope, $log, $location, $cookies, $http, $modal) {
    if (!$cookies.prisijunges) {
        $location.path('/');
    }

    $http({
        method: 'GET',
        url: '/api/v1/destytojas/grupes/' + $cookies.vardas + '/' + $cookies.slapt,
    }).then(function successCallback(response) {
        $scope.grupes = response.data;
    }, function errorCallback(response) {
    });

    $scope.prideti = function () {
        $http({
            method: 'POST',
            url: '/api/v1/apklausa/' + $cookies.vardas + '/' + $cookies.slapt,
            data: $scope.pranesimas
        }).then(function successCallback(response) {
        }, function errorCallback(response) {
        });

    };

    $scope.visosApklausos = function () {
        $modal.open({
            animation: true,
            templateUrl: 'views/visosApklausos.html',
            controller: 'visosApklausos',
        });

    };

    $scope.sukonstruokPavadinima = function (kursas, grupe, pogrupis) {
        pradzia = kursas + "k. " + grupe + " grupė ";
        return pogrupis > 0 ? pradzia + pogrupis + " pogrupis" : pradzia;
    };
});

app.controller('visosApklausos', function ($modalInstance, $scope, $log, $location, $cookies, $http, $rootScope) {
    if (!$cookies.prisijunges) {
        $location.path('/');
    }

    $http({
        method: 'GET',
        url: '/api/v1/destytojas/apklausa/' + $cookies.vardas,
    }).then(function successCallback(response) {
        $log.log(response.data);
        $scope.apklausos = response.data;
    }, function errorCallback(response) {
    });

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    }
});

app.controller('prisijungimo', function ($scope, $cookies, $cookieStore, $location, $log) {
    if (!$cookies.prisijunges) {
        $location.path('/');
    }
    $scope.prisijunges = $cookies.prisijunges;
    $scope.vardas = $cookies.vardas;

    $scope.atsijungti = function () {
        $cookieStore.remove('prisijunges');
        $cookieStore.remove('vardas');
        $cookieStore.remove('pavarde');
        $scope.prisijunges = false;
        $location.path('/');
    };
    $scope.$on("prisijungiau", function () {
        $scope.prisijunges = true;
        $scope.vardas = $cookies.vardas;
    });
    $scope.$on("nerVartotojo", function () {
        $scope.prisijunges = false;
    });
});

package lt.vu.mif.kalendorius.servisai;

import com.google.gson.Gson;
import lt.vu.mif.kalendorius.duomenys.Kontrolinis;
import lt.vu.mif.kalendorius.duomenys.PasirinktineVeikla;
import lt.vu.mif.kalendorius.duomenys.Studentas;
import lt.vu.mif.kalendorius.duomenys.Vartotojas;
import lt.vu.mif.kalendorius.duomenys.dto.KontrolinioDto;
import lt.vu.mif.kalendorius.duomenys.dto.VeiklosDto;
import lt.vu.mif.kalendorius.ekstraktoriai.Destytojai;
import lt.vu.mif.kalendorius.repozitorijos.PapildomuVeikluRepozitorija;
import lt.vu.mif.kalendorius.repozitorijos.VartotojuRepozitorija;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Set;
import java.util.stream.Collectors;

public class PapildomuVeikluServisas {
    private static PapildomuVeikluServisas instance = new PapildomuVeikluServisas();

    private PapildomuVeikluRepozitorija papildomuVeikluRepozitorija = PapildomuVeikluRepozitorija.getInstance();

    private VartotojuRepozitorija vartotojuRepozitorija = VartotojuRepozitorija.getInstance();

    private PapildomuVeikluServisas() {
    }

    public static PapildomuVeikluServisas getInstance() {
        if (instance == null) {
            return new PapildomuVeikluServisas();
        }
        return instance;
    }

    public void pridetiPasirinktineVeikla(String vartotojoVardas, String slaptazodis, String veikla) {
        VeiklosDto veiklosDto = new Gson().fromJson(veikla, VeiklosDto.class);
        LocalDateTime laikas = LocalDateTime.parse(veiklosDto.getMetai() + "-" + veiklosDto.getMenuo() + "-" + veiklosDto.getDiena() + "T" + veiklosDto.getValandos());
        Vartotojas savininkas = vartotojuRepozitorija.gauti(vartotojoVardas, slaptazodis);
        PasirinktineVeikla nauja = new PasirinktineVeikla(Collections.singletonList(savininkas), laikas, veiklosDto.getDetales(), veiklosDto.getVeikla());
        papildomuVeikluRepozitorija.pridetiPapildomaVeikla(nauja);
    }

    public void pridetiKontrolini(String pVardas, String slapt, String kontrolinis) {
        Vartotojas v = vartotojuRepozitorija.gauti(pVardas, slapt);

        KontrolinioDto kontrolinioDto = new Gson().fromJson(kontrolinis, KontrolinioDto.class);
        LocalDateTime laikas = LocalDateTime.parse(kontrolinioDto.getMetai() + "-" + kontrolinioDto.getMenuo() + "-" + kontrolinioDto.getDiena() + "T" + kontrolinioDto.getValandos());
        Set<Studentas> studentai = vartotojuRepozitorija.gautiPagalDto(kontrolinioDto.getGrupesDto());

        Kontrolinis naujas = new Kontrolinis(studentai.stream().collect(Collectors.toList()), laikas, kontrolinioDto.getDetales(), kontrolinioDto.getKabinetas(), kontrolinioDto.getPaskaitosPavadinimas(), Destytojai.sukonstruokVarda(v.getVardas(), v.getPavarde()));

        papildomuVeikluRepozitorija.pridetiPapildomaVeikla(naujas);
    }

    public void istrinti(int id) {
        papildomuVeikluRepozitorija.istrintiPapildomaVeikla(id);
    }
}

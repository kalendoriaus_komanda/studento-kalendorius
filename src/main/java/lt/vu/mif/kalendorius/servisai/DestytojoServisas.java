package lt.vu.mif.kalendorius.servisai;

import lt.vu.mif.kalendorius.duomenys.Paskaita;
import lt.vu.mif.kalendorius.duomenys.Studentas;
import lt.vu.mif.kalendorius.duomenys.Vartotojas;
import lt.vu.mif.kalendorius.duomenys.dto.DestytojoGrupesDto;
import lt.vu.mif.kalendorius.ekstraktoriai.Destytojai;
import lt.vu.mif.kalendorius.repozitorijos.PaskaituRepozitorija;
import lt.vu.mif.kalendorius.repozitorijos.VartotojuRepozitorija;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class DestytojoServisas {
    private static DestytojoServisas instance = null;
    private VartotojuRepozitorija vartotojuRepozitorija = VartotojuRepozitorija.getInstance();
    private PaskaituRepozitorija paskaituRepozitorija = PaskaituRepozitorija.getInstance();

    private DestytojoServisas() {

    }

    public static DestytojoServisas getInstance() {
        if (instance == null) {
            instance = new DestytojoServisas();
        }
        return instance;
    }

    public List<Studentas> gautiStudentus(String pVardas, String slapt) {
        Vartotojas dest = vartotojuRepozitorija.gauti(pVardas, slapt);

        List<Studentas> studentai = vartotojuRepozitorija.gautiVisus()
                .stream()
                .filter(vartotojas -> vartotojas instanceof Studentas)
                .map(vartotojas1 -> (Studentas) vartotojas1)
                .collect(Collectors.toList());

        List<Studentas> sioDestytojo = new ArrayList<>();
        studentai.stream().forEach(vartotojas -> {
            List<Paskaita> sioDestytojoPaskaitos = paskaituRepozitorija.gautiPaskaitas(vartotojas.getStudijuPrograma(), vartotojas.getKursas(), vartotojas.getGrupe()).stream()
                    .filter(paskaita -> paskaita.getDestytojas().contains(Destytojai.sukonstruokVarda(dest.getVardas(), dest.getPavarde()))).collect(Collectors.toList());
            if (sioDestytojoPaskaitos.size() > 0) {
                sioDestytojo.add(vartotojas);
            }
        });
        return sioDestytojo;
    }

    public Set<DestytojoGrupesDto> gautiGrupes(String pVardas, String slapt) {
        Vartotojas dest = vartotojuRepozitorija.gauti(pVardas, slapt);
        Set<DestytojoGrupesDto> destytojoPaskaitos =
        paskaituRepozitorija.gautiVisas()
                .stream()
                .filter(paskaita ->
                paskaita.getDestytojas().contains(Destytojai.sukonstruokVarda(dest.getVardas(), dest.getPavarde())) && paskaita.getKursas() != 0)
                .map(paskaita -> new DestytojoGrupesDto(paskaita))
                .collect(Collectors.toCollection(HashSet::new));

        return destytojoPaskaitos;
    }
}

package lt.vu.mif.kalendorius.servisai;

import lt.vu.mif.kalendorius.duomenys.Kontrolinis;
import lt.vu.mif.kalendorius.duomenys.Paskaita;
import lt.vu.mif.kalendorius.duomenys.Studentas;
import lt.vu.mif.kalendorius.duomenys.Vartotojas;
import lt.vu.mif.kalendorius.duomenys.dto.Diena;
import lt.vu.mif.kalendorius.ekstraktoriai.Destytojai;
import lt.vu.mif.kalendorius.repozitorijos.PapildomuVeikluRepozitorija;
import lt.vu.mif.kalendorius.repozitorijos.PaskaituRepozitorija;
import lt.vu.mif.kalendorius.repozitorijos.VartotojuRepozitorija;
import lt.vu.mif.kalendorius.util.DatosKonverteris;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class DienosServisas {

    private static DienosServisas instance = new DienosServisas();

    private VartotojuRepozitorija vartotojuRepozitorija = VartotojuRepozitorija.getInstance();

    private PaskaituRepozitorija paskaituRepozitorija = PaskaituRepozitorija.getInstance();

    private PapildomuVeikluRepozitorija papildomuVeikluRepozitorija = PapildomuVeikluRepozitorija.getInstance();

    private DienosServisas() {
    }

    public static DienosServisas getInstance() {
        if (instance == null) {
            return new DienosServisas();
        }
        return instance;
    }

    public Diena gautiDienaStudentui(String prisijungimoVardas, String slaptazodis) {
        if (!vartotojuRepozitorija.egzistuoja(prisijungimoVardas)) {
            throw new IllegalArgumentException("Tokio vartotojo nera!");
        }
        Vartotojas v = vartotojuRepozitorija.gauti(prisijungimoVardas, slaptazodis);
        if (!(v instanceof Studentas)) {
            throw new IllegalArgumentException("Vartotojas ne studentas");
        }

        List<Paskaita> visosPaskaitos = paskaituRepozitorija.gautiPaskaitas(((Studentas) v).getStudijuPrograma(), ((Studentas) v).getKursas(), ((Studentas) v).getGrupe());
        List<Paskaita> filtruotosPagalPogrupiIrDiena = visosPaskaitos.stream()
                .filter(paskaita -> ((paskaita.getPogrupis() == ((Studentas) v).getPogrupis()) || paskaita.getPogrupis() == 0) && paskaita.getData().equals(DatosKonverteris.siandien()))
                .collect(Collectors.toCollection(ArrayList::new));

        Diena sukurta = new Diena();
        sukurta.setPaskaitos(filtruotosPagalPogrupiIrDiena);
        sukurta.setDiena(DatosKonverteris.siandien());
        sukurta.setPasirinktinesVeiklos(papildomuVeikluRepozitorija.gautiSiosDienosPasirinktinesVeiklas(v));
        sukurta.setKontroliniai(papildomuVeikluRepozitorija.gautiSiosDienosKontrolinius((Studentas) v));

        return sukurta;
    }

    public Diena gautiDienaDestytojui(String prisijungimoVardas, String slaptazodis) {
        if (!vartotojuRepozitorija.egzistuoja(prisijungimoVardas)) {
            throw new IllegalArgumentException("Tokio vartotojo nera!");
        }
        Vartotojas v = vartotojuRepozitorija.gauti(prisijungimoVardas, slaptazodis);

        List<Paskaita> visosPaskaitos = paskaituRepozitorija.gautiPaskaitasDestytojui(v.getVardas(), v.getPavarde());
        List<Paskaita> filtruotosPagalDiena = visosPaskaitos.stream()
                .filter(paskaita -> paskaita.getData().equals(DatosKonverteris.siandien()))
                .collect(Collectors.toCollection(ArrayList::new));

        Diena sukurta = new Diena();

        sukurta.setPaskaitos(filtruotosPagalDiena);
        sukurta.setDiena(DatosKonverteris.siandien());
        sukurta.setPasirinktinesVeiklos(papildomuVeikluRepozitorija.gautiSiosDienosPasirinktinesVeiklas(v));
        sukurta.setKontroliniai(papildomuVeikluRepozitorija.gautiVisas().stream()
                .filter(papildomaVeikla -> papildomaVeikla instanceof Kontrolinis)
                .map(papildomaVeikla1 -> (Kontrolinis) papildomaVeikla1)
                .filter(kontrolinis -> kontrolinis.getDestytojas().equals(Destytojai.sukonstruokVarda(v.getVardas(), v.getPavarde())))
                .collect(Collectors.toList()));
        return sukurta;
    }
}

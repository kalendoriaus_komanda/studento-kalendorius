package lt.vu.mif.kalendorius.servisai;

import lt.vu.mif.kalendorius.duomenys.Studentas;
import lt.vu.mif.kalendorius.duomenys.Vartotojas;
import lt.vu.mif.kalendorius.repozitorijos.VartotojuRepozitorija;

public class VartotojuServisas {
    private static VartotojuServisas instance = new VartotojuServisas();

    private VartotojuRepozitorija vartotojuRepozitorija = VartotojuRepozitorija.getInstance();

    private VartotojuServisas() {
    }

    public static VartotojuServisas getInstance() {
        if (instance == null) {
            return new VartotojuServisas();
        }
        return instance;
    }

    public void pridetiStudenta(String vartotojoVardas, String slaptazodis, String vardas, String pavarde, String studijuPrograma, int kursas, int grupe, int pogrupis) {
        patikrinkArEgzistuoja(vartotojoVardas);

        Studentas studentas = new Studentas(vartotojoVardas, slaptazodis, vardas, pavarde, studijuPrograma, kursas, grupe, pogrupis);

        vartotojuRepozitorija.prideti(studentas);
    }

    public void pridetiDestytoja(String vartotojoVardas, String slaptazodis, String vardas, String pavarde) {
        patikrinkArEgzistuoja(vartotojoVardas);

        Vartotojas vartotojas = new Vartotojas(vartotojoVardas, slaptazodis, vardas, pavarde);

        vartotojuRepozitorija.prideti(vartotojas);
    }

    private void patikrinkArEgzistuoja(String vartotojoVardas) {
        if (vartotojuRepozitorija.egzistuoja(vartotojoVardas)) {
            throw new IllegalArgumentException("Toks vartotojo vardas egzistuoja!");
        }
    }

    public boolean egzistuoja(String vartotojoVardas, String slaptazodis) {
        return vartotojuRepozitorija.egzistuoja(vartotojoVardas, slaptazodis);
    }

    public void istrinti(int id) {
        vartotojuRepozitorija.istrinti(id);
    }
}

package lt.vu.mif.kalendorius.servisai;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import lt.vu.mif.kalendorius.duomenys.Apklausa;
import lt.vu.mif.kalendorius.duomenys.Studentas;
import lt.vu.mif.kalendorius.duomenys.Vartotojas;
import lt.vu.mif.kalendorius.duomenys.dto.ApklausosDto;
import lt.vu.mif.kalendorius.duomenys.dto.DestytojoGrupesDto;
import lt.vu.mif.kalendorius.duomenys.enums.Atsakymas;
import lt.vu.mif.kalendorius.ekstraktoriai.Destytojai;
import lt.vu.mif.kalendorius.repozitorijos.ApklausuRepozitorija;
import lt.vu.mif.kalendorius.repozitorijos.VartotojuRepozitorija;

import java.util.List;
import java.util.Set;

public class ApklausuServisas {
    private static ApklausuServisas instance = null;
    private VartotojuRepozitorija vartotojuRepozitorija = VartotojuRepozitorija.getInstance();
    private ApklausuRepozitorija apklausuRepozitorija = ApklausuRepozitorija.getInstance();

    private ApklausuServisas() {
    }

    public static ApklausuServisas getInstance() {
        if (instance == null) {
            instance = new ApklausuServisas();
        }
        return instance;
    }

    public void pridetiApklausa(String pVardas, String slapt, String apklausaJson) {
        JsonObject object = new JsonParser().parse(apklausaJson).getAsJsonObject();
        DestytojoGrupesDto destytojoGrupesDto = new Gson().fromJson(object.get("destytojoGrupesDto"), DestytojoGrupesDto.class);
        Set<Studentas> gavejai = vartotojuRepozitorija.gautiPagalDto(destytojoGrupesDto);
        Vartotojas v = vartotojuRepozitorija.gauti(pVardas, slapt);

        apklausuRepozitorija.pridetiApklausa(gavejai, Destytojai.sukonstruokVarda(v.getVardas(), v.getPavarde()), object.get("tekstas").getAsString());
    }

    public void atsakytiIApklausa(String vartotojoVardas, String atsakymas, int apklausosId) {
        Apklausa apklausa = apklausuRepozitorija.gautiApklausa(apklausosId);
        apklausa.getAtsakymai().put(vartotojoVardas, Atsakymas.valueOf(atsakymas));
        apklausuRepozitorija.atnaujinti(apklausa);
    }

    public List<ApklausosDto> gautiApklausasStudentui(String prisijungimoVardas, String slaptazodis) {
        Vartotojas s = vartotojuRepozitorija.gauti(prisijungimoVardas, slaptazodis);
        if (!(s instanceof Studentas)) {
            throw new IllegalArgumentException("Blogi parameterai!");
        }
        return apklausuRepozitorija.gautiStudentoApklausas((Studentas) s);
    }

    public List<Apklausa> gautiDestytojoApklausas(String vartotojoVardas) {
        return apklausuRepozitorija.gautiDestytojoApklausas(vartotojoVardas);
    }
}

package lt.vu.mif.kalendorius.servisai;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import lt.vu.mif.kalendorius.duomenys.Pranesimas;
import lt.vu.mif.kalendorius.duomenys.Studentas;
import lt.vu.mif.kalendorius.duomenys.Vartotojas;
import lt.vu.mif.kalendorius.duomenys.dto.DestytojoGrupesDto;
import lt.vu.mif.kalendorius.ekstraktoriai.Destytojai;
import lt.vu.mif.kalendorius.repozitorijos.PranesimuRepozitorija;
import lt.vu.mif.kalendorius.repozitorijos.VartotojuRepozitorija;

import java.util.Set;

public class PranesimuServisas {
    private VartotojuRepozitorija vartotojuRepozitorija = VartotojuRepozitorija.getInstance();
    private PranesimuRepozitorija pranesimuRepozitorija = PranesimuRepozitorija.getInstance();
    private static PranesimuServisas instance = null;

    private PranesimuServisas() {

    }

    public static PranesimuServisas getInstance() {
        if (instance == null) {
            instance = new PranesimuServisas();
        }
        return instance;
    }

    public void pridetiPranesima(String pVardas, String slapt,  String pranesimas) {
        JsonObject object = new JsonParser().parse(pranesimas).getAsJsonObject();
        DestytojoGrupesDto destytojoGrupesDto = new Gson().fromJson(object.get("destytojoGrupesDto"), DestytojoGrupesDto.class);
        Set<Studentas> gavejai = vartotojuRepozitorija.gautiPagalDto(destytojoGrupesDto);
        Vartotojas v = vartotojuRepozitorija.gauti(pVardas, slapt);
        pranesimuRepozitorija.pridetiPranesima(Destytojai.sukonstruokVarda(v.getVardas(), v.getPavarde()), gavejai, object.get("tekstas").getAsString());
    }

    public Set<Pranesimas> gautiStudentoPranesimus(String vartotojoVardas, String slaptazodis) {
        Vartotojas v = vartotojuRepozitorija.gauti(vartotojoVardas, slaptazodis);
        if (!(v instanceof Studentas)) {
            throw new IllegalArgumentException("Blogi parametrai!");
        }
        return pranesimuRepozitorija.gautiStudentoPranesimus((Studentas)v);
    }
}

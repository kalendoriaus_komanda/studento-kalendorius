package lt.vu.mif.kalendorius.ekstraktoriai;

public abstract class Destytojai {
    public static String sukonstruokVarda(String vardas, String pavarde) {
        return String.format("%s. %s", vardas.substring(0, 1), pavarde);
    }
}

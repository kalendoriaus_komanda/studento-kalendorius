package lt.vu.mif.kalendorius.ekstraktoriai;

import lt.vu.mif.kalendorius.duomenys.Paskaita;
import lt.vu.mif.kalendorius.duomenys.enums.SavaitesDiena;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class StudentoPaskaituEkstraktorius {

    public static List<Paskaita> gautiPaskaitas(String studijuPrograma, int kursas, int grupe) {
        List<Paskaita> paskaituSarasas = new ArrayList<>();
        try {
            Document doc = Jsoup.connect("http://mif.vu.lt/tvark/timetable/").get();
            Iterator<Element> elementai = doc.getElementsByClass("index").get(0).getElementsContainingText(studijuPrograma).iterator();
            String tvarkarasciuPuslapis = gaukPuslapi(studijuPrograma, kursas, grupe, elementai);
            Paskaita nauja = null;
            SavaitesDiena dabartine = null;

            if (tvarkarasciuPuslapis != null) {
                Document tvarkarastis = Jsoup.connect("http://mif.vu.lt/" + tvarkarasciuPuslapis).get();
                List<Node> lentelesDuomenys = tvarkarastis.getElementsByClass("timetable").get(0).getElementsByTag("tbody").get(0).childNodes();

                for (Node node : lentelesDuomenys) {
                    if (node.childNodes().size() > 0) {

                        for (Node childNode : node.childNodes()) {
                            if (childNode.childNodes().size() > 0) {

                                for (Node last : childNode.childNodes()) {
                                    if (last instanceof TextNode) {
                                        TextNode tekstas = (TextNode) last;
                                        if (savaitesDiena(tekstas.text())) {
                                            String savaitesDiena  = tekstas.text();
                                            if (savaitesDiena.equals("Trečiadienis")) {
                                                savaitesDiena  = "Treciadienis";
                                            }
                                            if (savaitesDiena.equals("Šeštadienis")) {
                                                savaitesDiena = "Sestadienis";
                                            }
                                            dabartine = SavaitesDiena.valueOf(savaitesDiena);
                                        }
                                        if (childNode.attributes().get("class").equals("time")) {
                                            nauja = sukurkPaskaita(studijuPrograma, kursas, grupe);
                                            nauja.setData(dabartine);
                                            nauja.setLaikas(tekstas.text());
                                        }
                                    } else if (childNode.attributes().get("class").equals("lecture")) {
                                        for (Node lectureNode : last.childNodes()) {
                                            if (lectureNode instanceof Element) {
                                                Element elementas = (Element) lectureNode;
                                                if (!elementas.tag().getName().equals("br")) {
                                                    if (elementas.attributes().get("class").equals("subject")) {
                                                        String savaite = elementas.childNode(0) instanceof TextNode ? ((TextNode) elementas.childNode(0)).text() : null;
                                                        String pavadinimas = ((TextNode) elementas.getElementsByTag("a").get(0).childNode(0)).text();
                                                        nauja.setPavadinimas(pavadinimas + " " + savaite);
                                                    } else if (elementas.attributes().get("class").equals("author")) {
                                                        nauja.setDestytojas(((TextNode) elementas.childNode(0).childNode(0)).text());
                                                    } else if (elementas.tag().getName().equals("span")) {
                                                        if (((TextNode)elementas.childNode(0)).text().contains("Pogrupis")) {
                                                            String pogrupis = ((TextNode)elementas.childNode(0)).text();
                                                            String [] split = pogrupis.split(": ");
                                                            nauja.setPogrupis(Integer.valueOf(split[1]));
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    } else if (childNode.attributes().get("class").equals("room") && ((Element) last).tag().getName().equals("a")) {
                                        nauja.setKabinetas(((TextNode) last.childNode(0)).text());
                                        paskaituSarasas.add(nauja);
                                    }


                                }
                            }
                        }
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return paskaituSarasas;
    }

    private static Paskaita sukurkPaskaita(String studijuPrograma, int kursas, int grupe) {
        Paskaita nauja = new Paskaita();
        nauja.setKursas(kursas);
        nauja.setGrupe(grupe);
        nauja.setStudijuPrograma(studijuPrograma);
        return nauja;
    }

    private static String gaukPuslapi(String studijuKryptis, int kursas, int grupe, Iterator<Element> elementai) {
        while (elementai.hasNext()) {
            Element el = elementai.next();
            if (el.tag().getName().equals("td")) {
                for (Node node :
                        el.childNodes()) {
                    if (node.nodeName().equals("a") && node.childNodes().iterator().next() instanceof TextNode) {
                        TextNode textNode = (TextNode) node.childNodes().iterator().next();
                        if (textNode.text().contains(generuokPilnaPavadinima(studijuKryptis, kursas, grupe))) {
                            return textNode.parent().attr("href");
                        }
                    }
                }
            }
        }
        return null;
    }

    private static String generuokPilnaPavadinima(String studijuKryptis, int kursas, int grupe) {
        return String.format("%s, %d k., %d gr.", studijuKryptis, kursas, grupe);
    }

    private static boolean savaitesDiena(String tekstas) {
        if (tekstas.equals("Trečiadienis")) {
            tekstas  = "Treciadienis";
        }
        if (tekstas.equals("Šeštadienis")) {
            tekstas = "Sestadienis";
        }
        for (SavaitesDiena savaitesDiena : SavaitesDiena.values()) {
            if (savaitesDiena.name().toLowerCase().equals(tekstas.toLowerCase())) return true;
        }
        return false;
    }
}

package lt.vu.mif.kalendorius.ekstraktoriai;

import lt.vu.mif.kalendorius.duomenys.Paskaita;
import lt.vu.mif.kalendorius.duomenys.enums.SavaitesDiena;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class DestytojoPaskaituEkstraktorius {
    public static List<Paskaita> gautiPaskaitas(String vardas, String pavarde) {
        Document doc = null;
        try {
            doc = Jsoup.connect("http://mif.vu.lt/tvark/timetable/professor/").get();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        String destytojoVardas = Destytojai.sukonstruokVarda(vardas, pavarde);
        String tvarkarasciuPuslapis = gaukPuslapi(destytojoVardas, doc);
        try {
            doc = Jsoup.connect("http://mif.vu.lt/" + tvarkarasciuPuslapis).get();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        Element tvarkarastis = doc.select(".timetable").first();
        List<Element> vidusLenteles = tvarkarastis.select("tbody").get(0).children();
        Iterator<Element> iterator = vidusLenteles.iterator();
        List<Paskaita> sukurtos = new ArrayList<>();
        Paskaita kuriama = null;
        SavaitesDiena dabartineDiena = null;

        while (iterator.hasNext()) {
            Element dabartinis = iterator.next();
            if (!(dabartinis.children().select("th").isEmpty())) {
                String savaitesDiena = dabartinis.children().select("th").get(0).text();
                if (savaitesDiena.equals("Trečiadienis")) {
                    savaitesDiena = "Treciadienis";
                }
                if (savaitesDiena.equals("Šeštadienis")) {
                    savaitesDiena = "Sestadienis";
                }
                dabartineDiena = SavaitesDiena.valueOf(savaitesDiena);
            }
            if (!(dabartinis.children().select(".time").isEmpty())) {
                kuriama = pridetiPaskaita(destytojoVardas, sukurtos, dabartineDiena);
                kuriama.setLaikas(dabartinis.children().select(".time").get(0).text());
            }
            if (!(dabartinis.children().select(".subject").isEmpty())) {
                kuriama.setPavadinimas(dabartinis.children().select(".subject").get(0).text());
            }
            if (!(dabartinis.children().select(".room").isEmpty())) {
                kuriama.setKabinetas(dabartinis.children().select(".room").get(0).select("a").get(0).textNodes().get(0).text());
            }
            System.out.print(dabartinis);
        }
        return sukurtos;
    }

    private static Paskaita pridetiPaskaita(String destytojoVardas, List<Paskaita> sukurtos, SavaitesDiena dabartineDiena) {
        Paskaita kuriama;
        kuriama = new Paskaita();
        kuriama.setDestytojas(destytojoVardas);
        kuriama.setData(dabartineDiena);
        sukurtos.add(kuriama);
        return kuriama;
    }

    private static String gaukPuslapi(String destytojoVardas, Document doc) {
        List<Element> elements = doc.getElementsContainingText(destytojoVardas);
        Element linkas = elements.get(elements.size() - 1);
        return linkas.attributes().get("href");
    }


}

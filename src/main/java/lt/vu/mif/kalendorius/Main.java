package lt.vu.mif.kalendorius;

import static lt.vu.mif.kalendorius.util.SparkRoutesProvider.setupEndpoints;
import static spark.SparkBase.staticFileLocation;

public class Main {

    public static void main(String[] args) {
        staticFileLocation("/public");
        setupEndpoints();
    }

}

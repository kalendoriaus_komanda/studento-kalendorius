package lt.vu.mif.kalendorius.util;

import java.sql.Connection;
import java.sql.DriverManager;

public class H2Util {

    private static final String DATABASE_URL = "jdbc:h2:~/kalendorius";

    public static void createDatabase() {
        try {
            Class.forName("org.h2.Driver");
            Connection conn = DriverManager.
                    getConnection(DATABASE_URL, "sa", "sa");
            conn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}

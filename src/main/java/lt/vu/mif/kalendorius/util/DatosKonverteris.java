package lt.vu.mif.kalendorius.util;

import lt.vu.mif.kalendorius.duomenys.enums.SavaitesDiena;

import java.time.LocalDate;
import java.time.temporal.WeekFields;

public class DatosKonverteris {
    public static SavaitesDiena siandien() {
        WeekFields weekFields = WeekFields.ISO;
        int diena = LocalDate.now().get(weekFields.dayOfWeek());
        return SavaitesDiena.gauti(diena);
    }
}

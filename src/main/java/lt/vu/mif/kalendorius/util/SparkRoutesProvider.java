package lt.vu.mif.kalendorius.util;

import lt.vu.mif.kalendorius.ekstraktoriai.DestytojoPaskaituEkstraktorius;
import lt.vu.mif.kalendorius.repozitorijos.PaskaituRepozitorija;
import lt.vu.mif.kalendorius.servisai.*;

import java.util.Collections;

import static spark.Spark.*;

public class SparkRoutesProvider {

    private static final String API_CONTEXT = "/api/v1";

    private static final PaskaituRepozitorija paskaituRepozitorija = PaskaituRepozitorija.getInstance();

    private static final VartotojuServisas vartotojuServisas = VartotojuServisas.getInstance();

    private static final DienosServisas dienosServisas = DienosServisas.getInstance();

    private static final PapildomuVeikluServisas papildomuVeikluServisas = PapildomuVeikluServisas.getInstance();

    private static final PranesimuServisas pranesimuServisas = PranesimuServisas.getInstance();

    private static final ApklausuServisas apklausuServisas = ApklausuServisas.getInstance();

    private static final DestytojoServisas destytojuServisas = DestytojoServisas.getInstance();

    public static void setupEndpoints() {
        setupGet();
        setupPost();
        setupDelete();
    }

    private static void setupGet() {
        get(API_CONTEXT + "/studentas/paskaitos/:studijuPrograma/:kursas/:grupe", (request, response) -> {
            return paskaituRepozitorija.gautiPaskaitas(request.params(":studijuPrograma"), Integer.valueOf(request.params(":kursas")), Integer.valueOf(request.params(":grupe")));
        }, new JsonTransformer());

        get(API_CONTEXT + "/studentas/diena/:vartotojoVardas/:slaptazodis", (request, response) -> {
            return dienosServisas.gautiDienaStudentui(request.params(":vartotojoVardas"), request.params(":slaptazodis"));
        }, new JsonTransformer());

        get(API_CONTEXT + "/vartotojas/egzistuoja/:vartotojoVardas/:slaptazodis", (request, response) -> {
            return vartotojuServisas.egzistuoja(request.params(":vartotojoVardas"), request.params(":slaptazodis"));
        }, new JsonTransformer());

        get(API_CONTEXT + "/studentas/pranesimas/:vartotojoVardas/:slaptazodis", (request, response) -> {
            return pranesimuServisas.gautiStudentoPranesimus(request.params(":vartotojoVardas"), request.params(":slaptazodis"));
        }, new JsonTransformer());

        get(API_CONTEXT + "/studentas/apklausa/:vartotojoVardas/:slaptazodis", (request, response) -> {
            return apklausuServisas.gautiApklausasStudentui(request.params(":vartotojoVardas"), request.params(":slaptazodis"));
        }, new JsonTransformer());

        get(API_CONTEXT + "/destytojas/apklausa/:vartotojoVardas", (request, response) -> {
            return apklausuServisas.gautiDestytojoApklausas(request.params(":vartotojoVardas"));
        }, new JsonTransformer());

        get(API_CONTEXT + "/destytojas/paskaitos/:vardas/:pavarde", (request, response) -> {
            return DestytojoPaskaituEkstraktorius.gautiPaskaitas(request.params(":vardas"), request.params(":pavarde"));
        }, new JsonTransformer());

        get(API_CONTEXT + "/destytojas/diena/:vartotojoVardas/:slaptazodis", (request, response) -> {
            return dienosServisas.gautiDienaDestytojui(request.params(":vartotojoVardas"), request.params(":slaptazodis"));
        }, new JsonTransformer());

        get(API_CONTEXT + "/destytojas/studentai/:vartotojoVardas/:slaptazodis", (request, response) -> {
            return destytojuServisas.gautiStudentus(request.params(":vartotojoVardas"), request.params(":slaptazodis"));
        }, new JsonTransformer());

        get(API_CONTEXT + "/destytojas/grupes/:vartotojoVardas/:slaptazodis", (request, response) -> {
            return destytojuServisas.gautiGrupes(request.params(":vartotojoVardas"), request.params(":slaptazodis"));
        }, new JsonTransformer());
    }

    private static void setupPost() {
        post(API_CONTEXT + "/studentas/registruoti/:vartotojoVardas/:slaptazodis/:vardas/:pavarde/:studijuPrograma/:kursas/:grupe/:pogrupis", (request, response) -> {
            vartotojuServisas.pridetiStudenta(request.params(":vartotojoVardas"), request.params(":slaptazodis"), request.params(":vardas"), request.params(":pavarde"), request.params(":studijuPrograma"),
                    Integer.valueOf(request.params(":kursas")), Integer.valueOf(request.params(":grupe")), Integer.valueOf(request.params(":pogrupis")));
            response.status(200);
            return response;
        });
        post(API_CONTEXT + "/destytojas/registruoti/:vartotojoVardas/:slaptazodis/:vardas/:pavarde", (request, response) -> {
            vartotojuServisas.pridetiDestytoja(request.params(":vartotojoVardas"), request.params(":slaptazodis"), request.params(":vardas"), request.params(":pavarde"));
            response.status(200);
            return response;
        });

        post(API_CONTEXT + "/vartotojas/pridetiVeikla/:vartotojoVardas/:slaptazodis", (request, response) -> {
            papildomuVeikluServisas.pridetiPasirinktineVeikla(request.params(":vartotojoVardas"), request.params(":slaptazodis"), request.body());
            response.status(200);
            return response;
        });

        post(API_CONTEXT + "/pranesimas/:vartotojoVardas/:slaptazodis", (request, response) -> {
            pranesimuServisas.pridetiPranesima(request.params(":vartotojoVardas"), request.params(":slaptazodis"), request.body());
            response.status(200);
            return response;
        });

        post(API_CONTEXT + "/apklausa/:vartotojoVardas/:slaptazodis", (request, response) -> {
            apklausuServisas.pridetiApklausa(request.params(":vartotojoVardas"), request.params(":slaptazodis"), request.body());
            response.status(200);
            return response;
        });

        post(API_CONTEXT + "/kontrolinis/:vartotojoVardas/:slaptazodis", (request, response) -> {
            papildomuVeikluServisas.pridetiKontrolini(request.params(":vartotojoVardas"), request.params(":slaptazodis"), request.body());
            response.status(200);
            return response;
        });

        post(API_CONTEXT + "/apklausa/atsakyti/:vartotojoVardas/:id/:atsakymas", (request, response) -> {
            apklausuServisas.atsakytiIApklausa(request.params(":vartotojoVardas"), request.params(":atsakymas"), Integer.valueOf(request.params(":id")));
            response.status(200);
            return response;
        });
    }

    private static void setupDelete() {
        delete(API_CONTEXT + "/papildomaVeikla/istrinti/:id", ((request, response) -> {
            papildomuVeikluServisas.istrinti(Integer.valueOf(request.params("id")));
            response.status(200);
            return response;
        }));
        delete(API_CONTEXT + "/vartotojas/istrinti/:id", ((request, response) -> {
            vartotojuServisas.istrinti(Integer.valueOf(request.params("id")));
            response.status(200);
            return response;
        }));
    }
}

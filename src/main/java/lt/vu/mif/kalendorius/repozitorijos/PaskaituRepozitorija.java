package lt.vu.mif.kalendorius.repozitorijos;

import lt.vu.mif.kalendorius.dao.PaskaituDao;
import lt.vu.mif.kalendorius.duomenys.Paskaita;
import lt.vu.mif.kalendorius.ekstraktoriai.Destytojai;
import lt.vu.mif.kalendorius.ekstraktoriai.DestytojoPaskaituEkstraktorius;
import lt.vu.mif.kalendorius.ekstraktoriai.StudentoPaskaituEkstraktorius;

import java.util.List;

public class PaskaituRepozitorija {

    private static PaskaituRepozitorija instance = null;
    private PaskaituDao paskaituDao = new PaskaituDao();

    private PaskaituRepozitorija() {
    }

    public static PaskaituRepozitorija getInstance() {
        if (instance == null) {
            instance = new PaskaituRepozitorija();
        }
        return instance;
    }

    public List<Paskaita> gautiPaskaitas(String studijuPrograma, int kursas, int grupe) {
        if (!paskaituDao.egzistuoja(studijuPrograma, kursas, grupe)) {
            List<Paskaita> duom = StudentoPaskaituEkstraktorius.gautiPaskaitas(studijuPrograma, kursas, grupe);
            for (Paskaita p : duom) {
                paskaituDao.prideti(p);
            }
            return duom;
        }
        return paskaituDao.gautiVisas(studijuPrograma, kursas, grupe);
    }

    public List<Paskaita> gautiPaskaitasDestytojui(String vardas, String pavarde) {

        if (!paskaituDao.egzistuoja(Destytojai.sukonstruokVarda(vardas, pavarde))) {
            List<Paskaita> duom = DestytojoPaskaituEkstraktorius.gautiPaskaitas(vardas, pavarde);
            for (Paskaita p : duom) {
                paskaituDao.prideti(p);
            }
            return duom;
        }
        return paskaituDao.gautiVisas(Destytojai.sukonstruokVarda(vardas, pavarde));
    }

    public List<Paskaita> gautiVisas() {
        return paskaituDao.gautiVisus();
    }

}

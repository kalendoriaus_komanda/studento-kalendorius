package lt.vu.mif.kalendorius.repozitorijos;

import lt.vu.mif.kalendorius.dao.ApklausuDao;
import lt.vu.mif.kalendorius.dao.VartotojuDao;
import lt.vu.mif.kalendorius.duomenys.Apklausa;
import lt.vu.mif.kalendorius.duomenys.Studentas;
import lt.vu.mif.kalendorius.duomenys.Vartotojas;
import lt.vu.mif.kalendorius.duomenys.dto.ApklausosDto;
import lt.vu.mif.kalendorius.ekstraktoriai.Destytojai;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class ApklausuRepozitorija {
    private static ApklausuRepozitorija instance = null;
    private ApklausuDao apklausuDao = new ApklausuDao();
    private VartotojuDao vartotojuDao = new VartotojuDao();

    private ApklausuRepozitorija() {

    }

    public static ApklausuRepozitorija getInstance() {
        if (instance == null) {
            instance = new ApklausuRepozitorija();
        }
        return instance;
    }

    public List<ApklausosDto> gautiStudentoApklausas(Studentas s) {
        List<ApklausosDto> apklausos = new ArrayList<>();
        s.getApklausos().stream().forEach(apklausa -> apklausos.add(new ApklausosDto(apklausa.getNuoKo(), apklausa.getTekstas(), apklausa.getId())));
        return apklausos;
    }

    public void pridetiApklausa(Set<Studentas> gavejai, String nuoKo, String tekstas) {
        Apklausa apklausa = new Apklausa(gavejai, nuoKo, tekstas);
        apklausuDao.prideti(apklausa);
    }

    public Apklausa gautiApklausa(int id) {
        return apklausuDao.gauti(id);
    }

    public List<Apklausa> gautiDestytojoApklausas(String destytojoVardas) {
        Vartotojas v = vartotojuDao.gauti(destytojoVardas);
        return apklausuDao.gautiVisus().stream().filter(apklausa -> apklausa.getNuoKo().equals(Destytojai.sukonstruokVarda(v.getVardas(), v.getPavarde()))).collect(Collectors.toCollection(ArrayList::new));
    }

    public void atnaujinti(Apklausa a) {
        apklausuDao.atnaujinti(a);
    }
}

package lt.vu.mif.kalendorius.repozitorijos;

import lt.vu.mif.kalendorius.dao.PapildomuVeikluDao;
import lt.vu.mif.kalendorius.duomenys.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class PapildomuVeikluRepozitorija {

    private static PapildomuVeikluRepozitorija instance = null;
    private PapildomuVeikluDao papildomuVeikluDao = new PapildomuVeikluDao();

    private PapildomuVeikluRepozitorija() {
    }

    public static PapildomuVeikluRepozitorija getInstance() {
        if (instance == null) {
            instance = new PapildomuVeikluRepozitorija();
        }
        return instance;
    }

    public <T> List<T> gautiPapildomaVeikla(Vartotojas s, Class<T> tipas) {
        List<PapildomaVeikla> papildomosVeiklos = papildomuVeikluDao.gautiPapildomasVeiklas();

        List<T> sioVartotojo = papildomosVeiklos.stream()
                .filter((papildomaVeikla ->
                        papildomaVeikla.getSavininkai().contains(s) && tipas.isAssignableFrom(papildomaVeikla.getClass())
                ))
                .map((papildomaVeikla -> (T) papildomaVeikla))
                .collect(Collectors.toCollection(ArrayList::new));

        return sioVartotojo;
    }

    public List<PasirinktineVeikla> gautiPasirinktinesVeiklas(Vartotojas s) {
        return gautiPapildomaVeikla(s, PasirinktineVeikla.class);
    }

    public List<PasirinktineVeikla> gautiSiosDienosPasirinktinesVeiklas(Vartotojas s) {
        List<PasirinktineVeikla> visos = gautiPapildomaVeikla(s, PasirinktineVeikla.class);
        return visos.stream()
                .filter(pasirinktineVeikla -> pasirinktineVeikla.getLaikas().toLocalDate().equals(LocalDate.now()))
                .collect(Collectors.toCollection(ArrayList::new));
    }

    public List<Kontrolinis> gautiKontrolinius(Studentas s) {
        return gautiPapildomaVeikla(s, Kontrolinis.class);
    }

    public List<Kontrolinis> gautiSiosDienosKontrolinius(Studentas s) {
        List<Kontrolinis> visos = gautiPapildomaVeikla(s, Kontrolinis.class);
        visos.stream()
                .filter(pasirinktineVeikla -> pasirinktineVeikla.getLaikas().toLocalDate().equals(LocalDate.now()))
                .collect(Collectors.toCollection(ArrayList::new));
        return visos;
    }

    public List<PapildomaVeikla> gautiVisas () {
        return papildomuVeikluDao.gautiPapildomasVeiklas();
    }

    public void pridetiPapildomaVeikla(PapildomaVeikla p) {
        papildomuVeikluDao.prideti(p);
    }

    public void istrintiPapildomaVeikla(int id) {
        papildomuVeikluDao.istrinti(id);
    }

    public void issaugoti(PapildomaVeikla p) {
        papildomuVeikluDao.atnaujinti(p);
    }
}

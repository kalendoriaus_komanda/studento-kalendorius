package lt.vu.mif.kalendorius.repozitorijos;

import lt.vu.mif.kalendorius.dao.PranesimuDao;
import lt.vu.mif.kalendorius.duomenys.Pranesimas;
import lt.vu.mif.kalendorius.duomenys.Studentas;

import java.util.List;
import java.util.Set;

public class PranesimuRepozitorija {
    private static PranesimuRepozitorija instance = null;
    private static PranesimuDao pranesimuDao =  new PranesimuDao();
    private PranesimuRepozitorija() {

    }

    public static PranesimuRepozitorija getInstance() {
        if (instance == null) {
            instance = new PranesimuRepozitorija();
        }
        return instance;
    }

    public Set<Pranesimas> gautiStudentoPranesimus(Studentas s) {
        return s.getGautiPranesimai();
    }

    public void pridetiPranesima(String nuoKo, Set<Studentas> gavejai, String tekstas) {
        Pranesimas pranesimas = new Pranesimas(gavejai, nuoKo, tekstas);
        pranesimuDao.prideti(pranesimas);
    }
}

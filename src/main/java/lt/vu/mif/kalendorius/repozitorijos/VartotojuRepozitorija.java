package lt.vu.mif.kalendorius.repozitorijos;

import lt.vu.mif.kalendorius.dao.VartotojuDao;
import lt.vu.mif.kalendorius.duomenys.PapildomaVeikla;
import lt.vu.mif.kalendorius.duomenys.Studentas;
import lt.vu.mif.kalendorius.duomenys.Vartotojas;
import lt.vu.mif.kalendorius.duomenys.dto.DestytojoGrupesDto;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class VartotojuRepozitorija {

    private static VartotojuRepozitorija instance = new VartotojuRepozitorija();
    private static final PapildomuVeikluRepozitorija papildomuVeikluRepozitorija = PapildomuVeikluRepozitorija.getInstance();
    private final VartotojuDao vartotojuDao = new VartotojuDao();

    private VartotojuRepozitorija() {

    }

    public static VartotojuRepozitorija getInstance() {
        if (instance == null) {
            instance = new VartotojuRepozitorija();
        }
        return instance;
    }

    public void prideti(Vartotojas s) {
        vartotojuDao.prideti(s);
    }

    public Vartotojas gauti(String vartotojoVardas, String slaptazodis) {
        if (!vartotojuDao.egzistuoja(vartotojoVardas)) {
            throw new IllegalArgumentException("Tokio vartotojo vardo nėra!");
        }
        return vartotojuDao.gauti(vartotojoVardas, slaptazodis);
    }

    public Vartotojas gauti(String vartotojoVardas) {
        if (!vartotojuDao.egzistuoja(vartotojoVardas)) {
            throw new IllegalArgumentException("Tokio vartotojo vardo nėra!");
        }
        return vartotojuDao.gauti(vartotojoVardas);
    }

    public boolean egzistuoja(String vartotojoVardas) {
        return vartotojuDao.egzistuoja(vartotojoVardas);
    }

    public boolean egzistuoja(String vartotojoVardas, String slaptazodis) {
        return vartotojuDao.egzistuoja(vartotojoVardas, slaptazodis);
    }

    public void istrinti(int id) {
        Vartotojas v = vartotojuDao.gauti(id);
        papildomuVeikluRepozitorija.gautiPapildomaVeikla(v, PapildomaVeikla.class).stream().forEach(papildomaVeikla -> {
            papildomaVeikla.getSavininkai().remove(v);
            papildomuVeikluRepozitorija.issaugoti(papildomaVeikla);
        });
        vartotojuDao.istrinti(id);
    }

    public Set<Studentas> gautiPagalDto(DestytojoGrupesDto destytojoGrupesDto) {
        return vartotojuDao.gautiVisus()
                .stream()
                .filter(vartotojas -> vartotojas instanceof Studentas)
                .map(vartotojas1 -> (Studentas) vartotojas1)
                .filter(studentas -> (destytojoGrupesDto.getPogrupis() == 0 || studentas.getPogrupis() == destytojoGrupesDto.getPogrupis())
                        && studentas.getGrupe() == destytojoGrupesDto.getGrupe()
                        && studentas.getKursas() == destytojoGrupesDto.getKursas())
                .collect(Collectors.toCollection(HashSet::new));
    }

    public List<Vartotojas> gautiVisus() {
        return vartotojuDao.gautiVisus();
    }
}

package lt.vu.mif.kalendorius.dao;

import lt.vu.mif.kalendorius.duomenys.Vartotojas;
import lt.vu.mif.kalendorius.util.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class VartotojuDao extends AbstraktusDao<Vartotojas> {

    private static final String VARDU_SKAICIUS = "Select count (*) from Vartotojas v where v.vartotojoVardas = :vartotojoVardas";
    private static final String VARDU_IR_SLAPTAZODZIU_SKAICIUS = "Select count (*) from Vartotojas v where v.vartotojoVardas = :vartotojoVardas and v.slaptazodis = :slaptazodis";
    private static final String GAUTI_VARTOTOJA = "Select v from Vartotojas v where v.vartotojoVardas = :vartotojoVardas and v.slaptazodis = :slaptazodis";
    private static final String GAUTI_VARTOTOJA_BE_SLAPTAZODZIO = "Select v from Vartotojas v where v.vartotojoVardas = :vartotojoVardas";
    private static final String ISTRINTI_VEIKLA = "Delete from veikla_vartotojas where savininkai_vartotojas_id = :id";

    public VartotojuDao() {
        super(Vartotojas.class);
    }

    public boolean egzistuoja(String prisijungimoVardas) {
        return egzistuoja(prisijungimoVardas, null);
    }

    public boolean egzistuoja(String prisijungimoVardas, String slaptazodis) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Query query = null;
            if (slaptazodis != null) {
                query = session.createQuery(VARDU_IR_SLAPTAZODZIU_SKAICIUS);
                query.setParameter("slaptazodis", slaptazodis);

            } else {
                query = session.createQuery(VARDU_SKAICIUS);
            }
            query.setParameter("vartotojoVardas", prisijungimoVardas);

            int kiekis = ((Long) query.uniqueResult()).intValue();
            tx.commit();
            if (kiekis > 0) {
                return true;
            } else {
                return false;
            }
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return false;
    }

    public Vartotojas gauti(String prisijungimoVardas, String slaptazodis) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Vartotojas v = null;
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery(GAUTI_VARTOTOJA);
            query.setParameter("vartotojoVardas", prisijungimoVardas);
            query.setParameter("slaptazodis", slaptazodis);
            v = (Vartotojas) query.list().get(0);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return v;
    }

    public Vartotojas gauti(String prisijungimoVardas) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Vartotojas v = null;
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery(GAUTI_VARTOTOJA_BE_SLAPTAZODZIO);
            query.setParameter("vartotojoVardas", prisijungimoVardas);
            v = (Vartotojas) query.list().get(0);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return v;
    }
}

package lt.vu.mif.kalendorius.dao;

import lt.vu.mif.kalendorius.duomenys.Apklausa;

public class ApklausuDao extends AbstraktusDao<Apklausa> {
    public ApklausuDao() {
        super(Apklausa.class);
    }
}

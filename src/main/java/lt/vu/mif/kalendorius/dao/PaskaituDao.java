package lt.vu.mif.kalendorius.dao;

import lt.vu.mif.kalendorius.duomenys.Paskaita;
import lt.vu.mif.kalendorius.util.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.ArrayList;
import java.util.List;

public class PaskaituDao extends AbstraktusDao<Paskaita> {

    private static final String PASKAITU_KIEKIS = "Select count (*) from Paskaita p where p.kursas = :kursas and p.grupe = :grupe and p.studijuPrograma = :studijuPrograma";
    private static final String GAUTI_PASKAITAS = "Select p from Paskaita p where p.kursas = :kursas and p.grupe = :grupe and p.studijuPrograma = :studijuPrograma";
    private static final String DESTYTOJO_PASKAITOS = "Select count (*) from Paskaita p where p.kursas = 0 and p.grupe = 0 and p.destytojas = :destVardas";
    private static final String GAUTI_VISAS_DESTYTOJO = "Select p  from Paskaita p where p.kursas = 0 and p.grupe = 0 and p.destytojas = :destVardas";

    public PaskaituDao() {
        super(Paskaita.class);
    }

    public boolean egzistuoja(String studijuPrograma, int kursas, int grupe) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery(PASKAITU_KIEKIS);
            query.setParameter("kursas", kursas);
            query.setParameter("grupe", grupe);
            query.setParameter("studijuPrograma", studijuPrograma);
            int kiekis = ((Long) query.uniqueResult()).intValue();
            tx.commit();
            if (kiekis > 0) {
                return true;
            } else {
                return false;
            }
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return false;
    }
    public boolean egzistuoja(String destytojoVardas) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery(DESTYTOJO_PASKAITOS);
            query.setParameter("destVardas", destytojoVardas);
            int kiekis = ((Long) query.uniqueResult()).intValue();
            tx.commit();
            if (kiekis > 0) {
                return true;
            } else {
                return false;
            }
        } catch (HibernateException e) {
             if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return false;
    }

    public List<Paskaita> gautiVisas(String studijuPrograma, int kursas, int grupe) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<Paskaita> duomenys = new ArrayList<>();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery(GAUTI_PASKAITAS);
            query.setParameter("kursas", kursas);
            query.setParameter("grupe", grupe);
            query.setParameter("studijuPrograma", studijuPrograma);
            duomenys = query.list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return duomenys;
    }

    public List<Paskaita> gautiVisas(String destytojoVardas) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<Paskaita> duomenys = new ArrayList<>();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery(GAUTI_VISAS_DESTYTOJO);
            query.setParameter("destVardas", destytojoVardas);
            duomenys = query.list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return duomenys;
    }
}

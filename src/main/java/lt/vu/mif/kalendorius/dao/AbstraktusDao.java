package lt.vu.mif.kalendorius.dao;

import lt.vu.mif.kalendorius.util.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class AbstraktusDao<T> {

    private final Class<T> type;

    public AbstraktusDao(Class<T> type) {
        this.type = type;
    }

    public void atnaujinti(T duomenys) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.update(duomenys);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    public T gauti(int id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        T duomenys = null;
        try {
            tx = session.beginTransaction();
            duomenys = (T)session.get(type, id);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return duomenys;
    }

    public Integer prideti(T duomenys) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        Integer id = null;
        try {
            tx = session.beginTransaction();
            id = (Integer) session.save(duomenys);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return id;
    }

    public List<T> gautiVisus() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        List<T> duomenys = null;
        try {
            tx = session.beginTransaction();
            duomenys = session.createQuery("FROM " + type.getSimpleName()).list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return duomenys;
    }

    public void istrinti(T duomenys) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.delete(duomenys);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    public void istrinti(int id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            T temp = gauti(id);
            session.delete(temp);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

}

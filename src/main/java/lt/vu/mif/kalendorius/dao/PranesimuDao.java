package lt.vu.mif.kalendorius.dao;

import lt.vu.mif.kalendorius.duomenys.Pranesimas;

public class PranesimuDao extends AbstraktusDao<Pranesimas> {

    public PranesimuDao() {
        super(Pranesimas.class);
    }

}

package lt.vu.mif.kalendorius.dao;

import lt.vu.mif.kalendorius.duomenys.PapildomaVeikla;

import java.util.List;

public class PapildomuVeikluDao extends AbstraktusDao<PapildomaVeikla> {

    public PapildomuVeikluDao() {
        super(PapildomaVeikla.class);
    }

    public void pridetiPapildomaVeikla(PapildomaVeikla p) {
        super.prideti(p);
    }

    public List<PapildomaVeikla> gautiPapildomasVeiklas() {
        return super.gautiVisus();
    }

}

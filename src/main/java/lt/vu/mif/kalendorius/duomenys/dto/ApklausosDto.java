package lt.vu.mif.kalendorius.duomenys.dto;

import lt.vu.mif.kalendorius.duomenys.enums.Atsakymas;

public class ApklausosDto {
    private String nuoKo;

    private String tekstas;

    private int id;

    public ApklausosDto(String nuoKo, String tekstas, int id) {
        this.nuoKo = nuoKo;
        this.tekstas = tekstas;
        this.id = id;
    }

    public String getNuoKo() {
        return nuoKo;
    }

    public void setNuoKo(String nuoKo) {
        this.nuoKo = nuoKo;
    }

    public String getTekstas() {
        return tekstas;
    }

    public void setTekstas(String tekstas) {
        this.tekstas = tekstas;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}

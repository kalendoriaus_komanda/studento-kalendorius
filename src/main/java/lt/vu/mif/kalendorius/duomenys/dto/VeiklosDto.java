package lt.vu.mif.kalendorius.duomenys.dto;

public class VeiklosDto {
    private String detales;
    private String veikla;
    private String metai;
    private String menuo;
    private String diena;
    private String valandos;

    public VeiklosDto() {
    }

    public String getDetales() {
        return detales;
    }

    public void setDetales(String detales) {
        this.detales = detales;
    }

    public String getVeikla() {
        return veikla;
    }

    public void setVeikla(String veikla) {
        this.veikla = veikla;
    }

    public String getMetai() {
        return metai;
    }

    public void setMetai(String metai) {
        this.metai = metai;
    }

    public String getMenuo() {
        return menuo;
    }

    public void setMenuo(String menuo) {
        this.menuo = menuo;
    }

    public String getDiena() {
        return diena;
    }

    public void setDiena(String diena) {
        this.diena = diena;
    }

    public String getValandos() {
        return valandos;
    }

    public void setValandos(String valandos) {
        this.valandos = valandos;
    }
}


package lt.vu.mif.kalendorius.duomenys;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class Kontrolinis extends PapildomaVeikla {

    String kabinetas;

    String paskaitosPavadinimas;

    String destytojas;

    public Kontrolinis(List<Vartotojas> savininkai, LocalDateTime laikas, String detales, String kabinetas, String paskaitosPavadinimas, String destytojas) {
        super(savininkai, laikas, detales);
        this.kabinetas = kabinetas;
        this.paskaitosPavadinimas = paskaitosPavadinimas;
        this.destytojas = destytojas;
    }

    public Kontrolinis() {
    }

    public String getKabinetas() {
        return kabinetas;
    }

    public void setKabinetas(String kabinetas) {
        this.kabinetas = kabinetas;
    }

    public String getPaskaitosPavadinimas() {
        return paskaitosPavadinimas;
    }

    public void setPaskaitosPavadinimas(String paskaitosPavadinimas) {
        this.paskaitosPavadinimas = paskaitosPavadinimas;
    }

    public String getDestytojas() {
        return destytojas;
    }

    public void setDestytojas(String destytojas) {
        this.destytojas = destytojas;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Kontrolinis that = (Kontrolinis) o;
        return Objects.equals(kabinetas, that.kabinetas) &&
                Objects.equals(paskaitosPavadinimas, that.paskaitosPavadinimas) &&
                Objects.equals(destytojas, that.destytojas);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), kabinetas, paskaitosPavadinimas, destytojas);
    }
}

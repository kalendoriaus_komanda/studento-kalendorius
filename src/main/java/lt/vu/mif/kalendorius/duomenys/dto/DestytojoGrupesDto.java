package lt.vu.mif.kalendorius.duomenys.dto;

import lt.vu.mif.kalendorius.duomenys.Paskaita;

import java.util.Objects;

public class DestytojoGrupesDto {
    private int grupe;
    private int pogrupis;
    private int kursas;

    public DestytojoGrupesDto() {
    }

    public DestytojoGrupesDto(Paskaita paskaita) {
        this.grupe = paskaita.getGrupe();
        this.pogrupis = paskaita.getPogrupis();
        this.kursas = paskaita.getKursas();
    }

    public int getGrupe() {
        return grupe;
    }

    public void setGrupe(int grupe) {
        this.grupe = grupe;
    }

    public int getPogrupis() {
        return pogrupis;
    }

    public void setPogrupis(int pogrupis) {
        this.pogrupis = pogrupis;
    }

    public int getKursas() {
        return kursas;
    }

    public void setKursas(int kursas) {
        this.kursas = kursas;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DestytojoGrupesDto that = (DestytojoGrupesDto) o;
        return grupe == that.grupe &&
                pogrupis == that.pogrupis &&
                kursas == that.kursas;
    }

    @Override
    public int hashCode() {
        return Objects.hash(grupe, pogrupis, kursas);
    }
}


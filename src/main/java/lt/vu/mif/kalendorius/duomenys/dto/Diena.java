package lt.vu.mif.kalendorius.duomenys.dto;

import lt.vu.mif.kalendorius.duomenys.*;
import lt.vu.mif.kalendorius.duomenys.enums.SavaitesDiena;

import java.util.List;

public class Diena {

    private SavaitesDiena diena;

    private List<Paskaita> paskaitos;

    private List<PasirinktineVeikla> pasirinktinesVeiklos;

    private List<Kontrolinis> kontroliniai;

    public Diena(SavaitesDiena diena, List<Paskaita> paskaitos, List<PasirinktineVeikla> pasirinktinesVeiklos, List<Kontrolinis> kontroliniai, Vartotojas savininkas) {
        this.diena = diena;
        this.paskaitos = paskaitos;
        this.pasirinktinesVeiklos = pasirinktinesVeiklos;
        this.kontroliniai = kontroliniai;
    }

    public Diena() {
    }

    public SavaitesDiena getDiena() {
        return diena;
    }

    public void setDiena(SavaitesDiena diena) {
        this.diena = diena;
    }

    public List<Paskaita> getPaskaitos() {
        return paskaitos;
    }

    public void setPaskaitos(List<Paskaita> paskaitos) {
        this.paskaitos = paskaitos;
    }

    public List<PasirinktineVeikla> getPasirinktinesVeiklos() {
        return pasirinktinesVeiklos;
    }

    public void setPasirinktinesVeiklos(List<PasirinktineVeikla> pasirinktinesVeiklos) {
        this.pasirinktinesVeiklos = pasirinktinesVeiklos;
    }

    public List<Kontrolinis> getKontroliniai() {
        return kontroliniai;
    }

    public void setKontroliniai(List<Kontrolinis> kontroliniai) {
        this.kontroliniai = kontroliniai;
    }

}

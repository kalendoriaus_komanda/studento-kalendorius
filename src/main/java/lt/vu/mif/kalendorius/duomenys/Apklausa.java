package lt.vu.mif.kalendorius.duomenys;

import lt.vu.mif.kalendorius.duomenys.enums.Atsakymas;

import javax.persistence.*;
import java.util.Map;
import java.util.Set;

@Entity
public class Apklausa {

    @GeneratedValue
    @Id
    private int id;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "Apklausa_gavejai", joinColumns = { @JoinColumn(name = "apklausa_id") }, inverseJoinColumns = { @JoinColumn(name = "studentas_id") })
    private Set<Studentas> gavejai;

    private String nuoKo;

    private String tekstas;

    @ElementCollection(fetch = FetchType.EAGER)
    @Enumerated(EnumType.STRING)
    private Map<String, Atsakymas> atsakymai;

    public Apklausa(Set<Studentas> gavejai, String nuoKo, String tekstas) {
        this.gavejai = gavejai;
        this.nuoKo = nuoKo;
        this.tekstas = tekstas;
    }

    public Apklausa() {
    }

    public Set<Studentas> getGavejai() {
        return gavejai;
    }

    public void setGavejai(Set<Studentas> gavejai) {
        this.gavejai = gavejai;
    }

    public String getNuoKo() {
        return nuoKo;
    }

    public void setNuoKo(String nuoKo) {
        this.nuoKo = nuoKo;
    }

    public String getTekstas() {
        return tekstas;
    }

    public void setTekstas(String tekstas) {
        this.tekstas = tekstas;
    }

    public Map<String, Atsakymas> getAtsakymai() {
        return atsakymai;
    }

    public int getId() {
        return id;
    }
}

package lt.vu.mif.kalendorius.duomenys.dto;

public class KontrolinioDto {
    private String kabinetas;
    private String paskaitosPavadinimas;
    private String metai;
    private String menuo;
    private String diena;
    private String valandos;
    private String detales;
    private DestytojoGrupesDto grupesDto;

    public KontrolinioDto() {
    }

    public String getKabinetas() {
        return kabinetas;
    }

    public void setKabinetas(String kabinetas) {
        this.kabinetas = kabinetas;
    }

    public String getPaskaitosPavadinimas() {
        return paskaitosPavadinimas;
    }

    public void setPaskaitosPavadinimas(String paskaitosPavadinimas) {
        this.paskaitosPavadinimas = paskaitosPavadinimas;
    }

    public String getMetai() {
        return metai;
    }

    public void setMetai(String metai) {
        this.metai = metai;
    }

    public String getMenuo() {
        return menuo;
    }

    public void setMenuo(String menuo) {
        this.menuo = menuo;
    }

    public String getDiena() {
        return diena;
    }

    public void setDiena(String diena) {
        this.diena = diena;
    }

    public String getValandos() {
        return valandos;
    }

    public void setValandos(String valandos) {
        this.valandos = valandos;
    }

    public String getDetales() {
        return detales;
    }

    public void setDetales(String detales) {
        this.detales = detales;
    }

    public DestytojoGrupesDto getGrupesDto() {
        return grupesDto;
    }

    public void setGrupesDto(DestytojoGrupesDto grupesDto) {
        this.grupesDto = grupesDto;
    }
}

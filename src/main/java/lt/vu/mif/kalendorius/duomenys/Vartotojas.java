package lt.vu.mif.kalendorius.duomenys;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Objects;

@Entity
public class Vartotojas{

    @GeneratedValue
    @Id
    @Column(name = "vartotojas_id")
    private int id;

    private String vartotojoVardas;

    private String slaptazodis;

    private String vardas;

    private String pavarde;

    public Vartotojas() {
    }

    public Vartotojas(String vartotojoVardas, String slaptazodis, String vardas, String pavarde) {
        this.vartotojoVardas = vartotojoVardas;
        this.slaptazodis = slaptazodis;
        this.vardas = vardas;
        this.pavarde = pavarde;
    }

    public int getId() {
        return id;
    }

    public String getVartotojoVardas() {
        return vartotojoVardas;
    }

    public void setVartotojoVardas(String vartotojoVardas) {
        this.vartotojoVardas = vartotojoVardas;
    }

    public String getSlaptazodis() {
        return slaptazodis;
    }

    public void setSlaptazodis(String slaptazodis) {
        this.slaptazodis = slaptazodis;
    }

    public String getVardas() {
        return vardas;
    }

    public void setVardas(String vardas) {
        this.vardas = vardas;
    }

    public String getPavarde() {
        return pavarde;
    }

    public void setPavarde(String pavarde) {
        this.pavarde = pavarde;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vartotojas that = (Vartotojas) o;
        return Objects.equals(vartotojoVardas, that.vartotojoVardas) &&
                Objects.equals(slaptazodis, that.slaptazodis) &&
                Objects.equals(vardas, that.vardas) &&
                Objects.equals(pavarde, that.pavarde);
    }

    @Override
    public int hashCode() {
        return Objects.hash(vartotojoVardas, slaptazodis, vardas, pavarde);
    }
}

package lt.vu.mif.kalendorius.duomenys.enums;

public enum SavaitesDiena {
    Pirmadienis (1),
    Antradienis (2),
    Treciadienis (3),
    Ketvirtadienis (4),
    Penktadienis (5),
    Sestadienis (6),
    Sekmadienis (7);

    private int indeksas;

    SavaitesDiena(int indeksas) {
        this.indeksas = indeksas;
    }

    public static SavaitesDiena gauti(int indeksas) {
        for (SavaitesDiena s : SavaitesDiena.values()) {
            if (s.indeksas == indeksas) return s;
        }
        return Pirmadienis;
    }
}


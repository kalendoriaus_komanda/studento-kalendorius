package lt.vu.mif.kalendorius.duomenys.enums;

public enum Atsakymas {
    Taip,
    Ne
}

package lt.vu.mif.kalendorius.duomenys;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class Studentas extends Vartotojas {

    private String studijuPrograma;

    private int kursas;

    private int grupe;

    private int pogrupis;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, mappedBy = "gavejai", fetch = FetchType.EAGER)
    private Set<Pranesimas> gautiPranesimai;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, mappedBy = "gavejai", fetch = FetchType.EAGER)
    private Set<Apklausa> apklausos;

    public Studentas(String vartotojoVardas, String slaptazodis, String vardas, String pavarde) {
        super(vartotojoVardas, slaptazodis, vardas, pavarde);
    }

    public Studentas(String vartotojoVardas, String slaptazodis, String vardas, String pavarde, String studijuPrograma, int kursas, int grupe, int pogrupis) {
        super(vartotojoVardas, slaptazodis, vardas, pavarde);
        this.studijuPrograma = studijuPrograma;
        this.kursas = kursas;
        this.grupe = grupe;
        this.pogrupis = pogrupis;
    }

    public Studentas() {
        super();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    public String getStudijuPrograma() {
        return studijuPrograma;
    }

    public void setStudijuPrograma(String studijuPrograma) {
        this.studijuPrograma = studijuPrograma;
    }

    public int getKursas() {
        return kursas;
    }

    public void setKursas(int kursas) {
        this.kursas = kursas;
    }

    public int getGrupe() {
        return grupe;
    }

    public void setGrupe(int grupe) {
        this.grupe = grupe;
    }

    public int getPogrupis() {
        return pogrupis;
    }

    public void setPogrupis(int pogrupis) {
        this.pogrupis = pogrupis;
    }

    public Set<Pranesimas> getGautiPranesimai() {
        return gautiPranesimai;
    }

    public Set<Apklausa> getApklausos() {
        return apklausos;
    }
}

package lt.vu.mif.kalendorius.duomenys;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class PasirinktineVeikla extends PapildomaVeikla {

    String veikla;

    public PasirinktineVeikla(List<Vartotojas> savininkai, LocalDateTime laikas, String detales, String veikla) {
        super(savininkai, laikas, detales);
        this.veikla = veikla;
    }

    public PasirinktineVeikla() {
    }

    public String getVeikla() {
        return veikla;
    }

    public void setVeikla(String veikla) {
        this.veikla = veikla;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        PasirinktineVeikla that = (PasirinktineVeikla) o;
        return Objects.equals(veikla, that.veikla);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), veikla);
    }
}

package lt.vu.mif.kalendorius.duomenys;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

@Entity
public abstract class PapildomaVeikla {

    @Id
    @GeneratedValue
    private int id;

    @ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.DETACH})
    @JoinTable(name="veikla_vartotojas")
    private List<Vartotojas> savininkai;

    private LocalDateTime laikas;

    private String detales;

    public PapildomaVeikla() {
    }

    public PapildomaVeikla(List<Vartotojas> savininkai, LocalDateTime laikas, String detales) {
        this.savininkai = savininkai;
        this.laikas = laikas;
        this.detales = detales;
    }

    public List<Vartotojas> getSavininkai() {
        return savininkai;
    }

    public void setSavininkai(List<Vartotojas> savininkai) {
        this.savininkai = savininkai;
    }

    public LocalDateTime getLaikas() {
        return laikas;
    }

    public void setLaikas(LocalDateTime laikas) {
        this.laikas = laikas;
    }

    public String getDetales() {
        return detales;
    }

    public void setDetales(String detales) {
        this.detales = detales;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PapildomaVeikla that = (PapildomaVeikla) o;
        return Objects.equals(savininkai, that.savininkai) &&
                Objects.equals(laikas, that.laikas) &&
                Objects.equals(detales, that.detales);
    }

    @Override
    public int hashCode() {
        return Objects.hash(savininkai, laikas, detales);
    }
}

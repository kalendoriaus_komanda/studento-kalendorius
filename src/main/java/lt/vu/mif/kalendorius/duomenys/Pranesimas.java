package lt.vu.mif.kalendorius.duomenys;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
public class Pranesimas {

    @GeneratedValue
    @Id
    private int id;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "Pranesimas_gavejai", joinColumns = { @JoinColumn(name = "pranesimas_id") }, inverseJoinColumns = { @JoinColumn(name = "studentas_id") })
    private Set<Studentas> gavejai;

    private String nuoKo;

    private String tekstas;

    public Pranesimas(Set<Studentas> gavejai, String nuoKo, String tekstas) {
        this.gavejai = gavejai;
        this.nuoKo = nuoKo;
        this.tekstas = tekstas;
    }

    public Pranesimas() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Set<Studentas> getGavejai() {
        return gavejai;
    }

    public void setGavejai(Set<Studentas> gavejai) {
        this.gavejai = gavejai;
    }

    public String getNuoKo() {
        return nuoKo;
    }

    public void setNuoKo(String nuoKo) {
        this.nuoKo = nuoKo;
    }

    public String getTekstas() {
        return tekstas;
    }

    public void setTekstas(String tekstas) {
        this.tekstas = tekstas;
    }
}

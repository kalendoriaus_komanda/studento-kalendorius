package lt.vu.mif.kalendorius.duomenys;

import lt.vu.mif.kalendorius.duomenys.enums.SavaitesDiena;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Objects;

@Entity
public class Paskaita {

    @Id
    @GeneratedValue
    private int id;

    private SavaitesDiena data;

    private String laikas;

    private String pavadinimas;

    private String kabinetas;

    private String studijuPrograma;

    private String destytojas;

    private int kursas;

    private int grupe;

    private int pogrupis;

    public Paskaita() {
    }

    public Paskaita(SavaitesDiena data, String pavadinimas, String kabinetas) {
        this.data = data;
        this.pavadinimas = pavadinimas;
        this.kabinetas = kabinetas;
    }

    public Paskaita(SavaitesDiena data, String pavadinimas, String kabinetas, String studijuPrograma, int kursas, int grupe, int pogrupis) {
        this.data = data;
        this.pavadinimas = pavadinimas;
        this.kabinetas = kabinetas;
        this.studijuPrograma = studijuPrograma;
        this.kursas = kursas;
        this.grupe = grupe;
        this.pogrupis = pogrupis;
    }

    public SavaitesDiena getData() {
        return data;
    }

    public void setData(SavaitesDiena data) {
        this.data = data;
    }

    public String getPavadinimas() {
        return pavadinimas;
    }

    public void setPavadinimas(String pavadinimas) {
        this.pavadinimas = pavadinimas;
    }

    public String getKabinetas() {
        return kabinetas;
    }

    public void setKabinetas(String kabinetas) {
        this.kabinetas = kabinetas;
    }

    public String getStudijuPrograma() {
        return studijuPrograma;
    }

    public void setStudijuPrograma(String studijuPrograma) {
        this.studijuPrograma = studijuPrograma;
    }

    public int getKursas() {
        return kursas;
    }

    public void setKursas(int kursas) {
        this.kursas = kursas;
    }

    public int getGrupe() {
        return grupe;
    }

    public void setGrupe(int grupe) {
        this.grupe = grupe;
    }

    public int getPogrupis() {
        return pogrupis;
    }

    public void setPogrupis(int pogrupis) {
        this.pogrupis = pogrupis;
    }

    public String getLaikas() {
        return laikas;
    }

    public String getDestytojas() {
        return destytojas;
    }

    public void setDestytojas(String destytojas) {
        this.destytojas = destytojas;
    }

    public void setLaikas(String laikas) {
        this.laikas = laikas;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Paskaita paskaita = (Paskaita) o;
        return id == paskaita.id &&
                kursas == paskaita.kursas &&
                grupe == paskaita.grupe &&
                pogrupis == paskaita.pogrupis &&
                data == paskaita.data &&
                Objects.equals(laikas, paskaita.laikas) &&
                Objects.equals(pavadinimas, paskaita.pavadinimas) &&
                Objects.equals(kabinetas, paskaita.kabinetas) &&
                Objects.equals(studijuPrograma, paskaita.studijuPrograma);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, data, laikas, pavadinimas, kabinetas, studijuPrograma, kursas, grupe, pogrupis);
    }

    public int getId() {
        return id;
    }

}
